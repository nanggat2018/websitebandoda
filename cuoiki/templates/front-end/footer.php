<footer>
    <div class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-5 col-sm-12">
                    <div class="footer-widget">
                        <div class="footer-logo">
                            <a href="#"><img style="width: 100px" src="admin/public/image_san_pham/logo.png"></a>
                        </div>
                        <div class="footer-contact">
                            <p><span>số điện thoại: </span>123 456 7890</p>
                            <p><span>email : </span>demo@example.com</p>
                        </div>
                        <div class="footer-icon">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <div class="footer-widget">
                        <a href="#">TÀI KHOẢN CỦA TÔI</a>
                        <a href="#">Đăng nhập</a>
                        <a href="#">Giỏ hàng của tôi</a>
                        <a href="#">Danh sách yêu thích</a>
                        <a href="#">Thủ tục thanh toán</a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="footer-widget">
                        <a href="#">VỀ LEATHER</a>
                        <a href="#">Giới thiệu</a>
                        <a href="#">Thông tin liên hệ</a>
                        <a href="#">Chính sách bảo mật</a>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="footer-widget">
                        <div class="footer-text">
                            <h4 class="title">form đăng kí</h4>
<!--                            <p>* Get the last news and special offers</p>-->
                        </div>
                        <div class="footer-newsletter">
                            <form action="#">
                                <input type="text" name="hoTen" placeholder="Nhập vào họ và tên">
<!--                                <span class="fa fa-envelope-o"></span>-->
<!--                                <button>Send</button>-->
                            </form>
                        </div>

                        <div class="footer-newsletter">
                            <form action="#">
                                <input type="text" name="email" placeholder="Email để nhận ưu đãi">
                                <span class="fa fa-envelope-o"></span>
                                <!--                                <button>Send</button>-->
                            </form>
                        </div>

                        <div class="footer-newsletter">
                            <form action="#">
                                <button>Đăng kí</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="copy-right-area">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-lg-6 col-md-8">
                    <div class="copy">
                        <p>&copy; 2022 <b>Leather</b> trực thuộc công ty <i class="fa fa-heart text-danger"></i> <b><a href="https://www.hasthemes.com/" target="_blank">Hoa Tiêu</a></b></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="payment">
                        <a href="#"><img src="public/layout/assets/images/payment/1.webp" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer Area -->
<!-- End wraper -->
</div>
<!-- start scrollUp
    ============================================ -->
<div id="toTop">
    <i class="fa fa-chevron-up"></i>
</div>
<!-- end scrollUp
    ============================================ -->





<!-- JS
============================================ -->

<!-- Modernizer & jQuery JS -->
<script src="public/layout/assets/js/vendor/jquery-1.12.4.min.js"></script>
<script src="public/layout/assets/js/vendor/modernizr-3.11.2.min.js"></script>

<!-- Nivo Slider JS -->
<script src="public/layout/assets/custom-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
<script src="public/layout/assets/custom-slider/home.js" type="text/javascript"></script>

<!-- Bootstrap JS -->
<script src="public/layout/assets/js/popper.min.js"></script>
<script src="public/layout/assets/js/bootstrap.min.js"></script>

<!-- Plugins JS -->
<script src="public/layout/assets/js/owl.carousel.min.js"></script>
<script src="public/layout/assets/js/jquery.meanmenu.js"></script>
<script src="public/layout/assets/js/jquery.collapse.js"></script>
<script src="public/layout/assets/js/jquery-ui.min.js"></script>
<script src="public/layout/assets/js/wow.min.js"></script>

<!-- Main JS -->
<script src="public/layout/assets/js/main.js"></script>

<script src="public/layout/assets/js/ajax_cart.js"></script>
<script src="public/layout/assets/js/Ajax_gia_sp.js"></script>
<script src="public/layout/assets/js/ajax_xoa_cart.js"></script>


</body>


<!-- Mirrored from template.hasthemes.com/sassy-girl-v1/sassy-girl/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 06 Dec 2021 03:30:13 GMT -->
</html>