

<div class="page-wrapper">

    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <form class="form-horizontal" enctype="multipart/form-data" method="POST">
                        <div class="card-body">
                            <h4 class="card-title">Sửa sản phẩm thanh toán<menu></menu></h4>
                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                                <div class="col-sm-9">
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="trang_thai" id="trang_thai">
                                        <option value="<?php echo $san_pham_thanh_toan_detail->trang_thai; ?>"><?php echo $san_pham_thanh_toan_detail->trang_thai ? "Mở" : "Khóa"; ?></option>
                                        <option value="1">Mở</option>
                                        <option value="0">Khóa</option>
                                    </select>
                                </div>
                            </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">ID san phẩm</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="id_san_pham" name="id_san_pham" value="<?php echo $san_pham_thanh_toan_detail->id_san_pham; ?>" placeholder="ID sản phẩm">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Giá tiền</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="gia_tien" name="gia_tien" value="<?php echo $san_pham_thanh_toan_detail->gia_tien; ?>" placeholder="Giá tiền" min=0>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">ID hóa đơn</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="id_hoa_don" name="id_hoa_don" value="<?php echo $san_pham_thanh_toan_detail->id_hoa_don; ?>">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">

                                <!--                            <form  action="../c_san_pham.php" method="POST">-->


                                <button type="submit" name="btn-submit" class="btn btn-primary">Sưa</button>

                                <!--                            </form>-->

                                <!--                            <button type="submit" class="btn btn-primary">Submit</button>-->
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- editor -->

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
</div>










