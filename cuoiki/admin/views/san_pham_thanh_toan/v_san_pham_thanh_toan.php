<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
               <div class="card">
        <div class="card-body">
            <h5 class="card-title">Sản phẩm thanh toán</h5>
            <div class="table-responsive">
                <table id="zero_config" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Trạng thái</th>
                        <th>id sản phẩm</th>
                        <th>Giá tiền</th>
                        <th>Hóa đơn</th>
                        <th>Hành động</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($san_pham_thanh_toans as $key=>$value){
                        $css_trang_thai = $value->trang_thai ? "badge-info": "badge-danger";
                        $text_trang_thai = $value->trang_thai ? "Mở" : "Khóa";

                    ?>
                    <tr>
                        <td><?php echo $value->id; ?></td>
                        <td><span class="badge badge-pill <?php echo $css_trang_thai; ?>"><?php echo $text_trang_thai; ?></span></td>
                        <td><?php echo $value->id_san_pham; ?></td>
                        <td><?php echo $value->gia_tien; ?></td>
                        <td><?php echo $value->id_hoa_don; ?></td>
                        <td><button type="button" class="btn btn-cyan btn-sm" onclick="window.location.href='edit_san_pham_thanh_toan.php?id=<?php echo $value->id;?>'">Edit</button>
                        <button type="button" class="btn btn-danger btn-sm" onclick="window.location.href='delete_san_pham_thanh_toan.php?id=<?php echo $value->id;?>'">Delete</button></td>
                    </tr>
                    <?php
                    }
                    ?>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
            </div>
        </div>
    </div>
</div>