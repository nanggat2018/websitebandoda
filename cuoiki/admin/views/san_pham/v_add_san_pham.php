
<div class="page-wrapper">

<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <form class="form-horizontal" enctype="multipart/form-data" method="POST">
                    <div class="card-body">
                        <h4 class="card-title">Thêm sản phẩm</h4>
                        <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên sản phẩm</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="ten_san_pham" name="ten_san_pham" placeholder="Tên sản phẩm">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                                <div class="col-sm-9">
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="trang_thai" id="trang_thai">
                                        <option>---Chọn---</option>
                                        <option value="1">Mở</option>
                                        <option value="0">Khóa</option>
                                     </select>
                                </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Mô tả chi tiết</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="mo_ta_chi_tiet" name="mo_ta_chi_tiet" placeholder="Mô tả chi tiết">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Giá tiền</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" id="gia_tien" name="gia_tien" placeholder="Giá tiền" min=0>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Số lượng </label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" id="so_luong" name="so_luong" value=0 min=0>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Hình ảnh</label>
                                <div class="col-sm-9">
                                    <div class="custom-file">
                                        <input type="file" name="hinh_anh" class="custom-file-input" id="validatedCustomFile" required>
                                        <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                        <div class="invalid-feedback">Example invalid custom file feedback</div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Hình ảnh hover</label>
                                <div class="col-sm-9">
                                    <div class="custom-file">
                                        <input type="file" name="hinh_anh_hover" class="custom-file-input" id="validatedCustomFile" required>
                                        <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                        <div class="invalid-feedback">Example invalid custom file feedback</div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Người đăng</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="nguoi_dang" name="nguoi_dang" placeholder="Người đăng">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Thời gian đăng</label>
                                <div class="col-sm-9">
                                    <input type="datetime-local" class="form-control" id="thoi_gian_dang" name="thoi_gian_dang" placeholder="Thời gian đăng">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Danh mục sản phẩm</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="danh_muc_san_pham" name="danh_muc_san_pham" placeholder="Danh mục sản phẩm">
                                </div>
                            </div>

                            </div>
                        </div>
                    <div class="border-top">
                        <div class="card-body">

<!--                            <form  action="../c_san_pham.php" method="POST">-->


                                <button type="submit" name="btn_submit" class="btn btn-primary">Submit</button>

<!--                            </form>-->

<!--                            <button type="submit" class="btn btn-primary">Submit</button>-->
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- editor -->

    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
