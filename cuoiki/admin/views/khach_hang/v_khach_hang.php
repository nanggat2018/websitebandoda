<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Khách hàng</h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Tên khách hàng</th>
                                    <th>Trạng thái</th>
                                    <th>Ngày sinh</th>
                                    <th>Địa chỉ</th>
                                    <th>Số điện thoại</th>
                                    <th>Email</th>
                                    <th>Hành động</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($khach_hang as $key=>$value){
                                    $css_trang_thai = $value->trang_thai ? "badge-info" : "badge-danger";
                                    $text_trang_thai = $value->trang_thai ? "Mở" : "Khóa";

                                    ?>
                                    <tr>
                                        <td><?php echo $value->ten_khach_hang; ?></td>
                                        <td><span class="badge badge-pill <?php echo $css_trang_thai; ?>"><?php echo $text_trang_thai; ?></span></td>

                                        <!--                                        <td><img width="100px" src="public/imagebanner/--><?php //echo $value->hinh;?><!--"></td>-->
                                        <td><?php echo $value->ngay_sinh; ?></td>
                                        <td><?php echo $value->dia_chi; ?></td>
                                        <td><?php echo $value->so_dien_thoai; ?></td>
                                        <td><?php echo $value->email; ?></td>

                                        <td><button type="button" class="btn btn-cyan btn-sm" onclick="window.location.href='edit_khach_hang.php?id=<?php echo $value->id;?>'">Edit</button>
                                            <button type="button" style="margin-left: 15px;" name="delete" class="btn btn-danger btn-sm" style="margin-left: 10px;" onclick="window.location.href='delete_khach_hang.php?id=<?php echo $value->id;?>' " >Delete</button></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>