
<div class="page-wrapper">

    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <form class="form-horizontal" enctype="multipart/form-data" method="POST">
                        <div class="card-body">
                            <h4 class="card-title">Sửa khách hàng</h4>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên khách hàng</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="ten_khach_hang" name="ten_khach_hang" value="<?php echo $khach_hang_detail->ten_khach_hang;?>" >
                                </div>
                            </div>

                            <div class="form-group row" >
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                                <div class="col-sm-9">
                                    <?php
                                    $text_trang_thai = $khach_hang_detail->trang_thai ? "Mở" : "Đóng"; ?>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="trang_thai" id="trang_thai">
                                        <option value="<?php echo $khach_hang_detail->trang_thai;?>"><?php echo $text_trang_thai;?></option>
                                        <option value="1">Mở</option>
                                        <option value="0">Đóng</option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Ngày sinh</label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" id="ngay_sinh" name="ngay_sinh" value="<?php echo $khach_hang_detail->ngay_sinh;?>" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Địa chỉ</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="dia_chi" name="dia_chi" value="<?php echo $khach_hang_detail->dia_chi;?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Số điện thoại</label>
                                <div class="col-sm-9">
                                    <input type="number" class="form-control" id="so_dien_thoai" name="so_dien_thoai" value="<?php echo $khach_hang_detail->so_dien_thoai;?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="email" name="email" value="<?php echo $khach_hang_detail->email;?>">
                                </div>
                            </div>





                        </div>
                        <div class="border-top">
                            <div class="card-body">

                                <!--                            <form  action="../c_banner.php" method="POST">-->


                                <button type="submit" name="btn_submit" class="btn btn-primary">Submit</button>

                                <!--                            </form>-->

                                <!--                            <button type="submit" class="btn btn-primary">Submit</button>-->
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- editor -->

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
