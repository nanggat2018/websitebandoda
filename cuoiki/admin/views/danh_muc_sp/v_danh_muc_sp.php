<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Danh mục sản phẩm</h5>
                        <div class="table-responsive">
                            <button type="button" class="btn btn-cyan btn-sm" onclick="window.location.href='add_danh_muc_sp.php'">Add</button>
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Tên danh mục</th>
                                    <th>Trạng thái</th>
                                    <th>Người đăng</th>
                                    <th>Thời gian đăng</th>
                                    <th>Người chỉnh sửa</th>
                                    <th>Thời gian sửa</th>
                                    <th>Hành động</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($danh_muc_sp as $key=>$value){
                                    $css_trang_thai = $value->trang_thai ? "badge-info" : "badge-danger";
                                    $text_trang_thai = $value->trang_thai ? "Mở" : "Khóa";

                                    ?>
                                    <tr>
                                        <td><?php echo $value->ten_danh_muc; ?></td>
                                        <td><span class="badge badge-pill <?php echo $css_trang_thai; ?>"><?php echo $text_trang_thai; ?></span></td>

                                        <!--                                        <td><img width="100px" src="public/imagebanner/--><?php //echo $value->hinh;?><!--"></td>-->
                                        <td><?php echo $value->nguoi_dang; ?></td>
                                        <td><?php echo $value->thoi_gian_dang; ?></td>
                                        <td><?php echo $value->nguoi_chinh_sua; ?></td>
                                        <td><?php echo $value->thoi_gian_sua; ?></td>
                                        <td><button type="button" class="btn btn-cyan btn-sm" onclick="window.location.href='edit_danh_muc_sp.php?id=<?php echo $value->id;?>'">Edit</button>
                                            <button type="button" name="delete" class="btn btn-danger btn-sm" style="margin-top: 10px;" onclick="window.location.href='delete_danh_muc_sp.php?id=<?php echo $value->id;?>' " >Delete</button></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>