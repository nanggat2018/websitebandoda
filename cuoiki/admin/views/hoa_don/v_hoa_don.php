<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Hóa đơn</h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Trạng thái</th>
                                    <th>Số lượng</th>
                                    <th>Id khách hàng</th>
                                    <th>Hành động</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($hoa_don as $key=>$value){
                                    $css_trang_thai = $value->trang_thai ? "badge-info" : "badge-danger";
                                    $text_trang_thai = $value->trang_thai ? "Mở" : "Khóa";

                                    ?>
                                    <tr>
<!--                                        <td>--><?php //echo $value->ten_danh_muc; ?><!--</td>-->
                                        <td><span class="badge badge-pill <?php echo $css_trang_thai; ?>"><?php echo $text_trang_thai; ?></span></td>

                                        <!--                                        <td><img width="100px" src="public/imagebanner/--><?php //echo $value->hinh;?><!--"></td>-->
                                        <td><?php echo $value->so_luong; ?></td>
                                        <td><?php echo $value->id_khach_hang; ?></td>
                                        <td><button type="button" class="btn btn-cyan btn-sm" onclick="window.location.href='edit_hoa_don.php?id=<?php echo $value->id;?>'">Edit</button>
                                            <button type="button" name="delete" class="btn btn-danger btn-sm" style="margin-left: 10px;" onclick="window.location.href='delete_hoa_don.php?id=<?php echo $value->id;?>' " >Delete</button></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>