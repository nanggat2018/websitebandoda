<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Mã khuyến mại</h5>
                        <div class="table-responsive">
                            <button type="button" class="btn btn-cyan btn-sm" onclick="window.location.href='add_ma_khuyen_mai.php'">Add</button>
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Tên mã khuyến mại</th>
                                    <th>Trạng thái</th>
                                    <th>Người đăng</th>
                                    <th>Thời gian đăng</th>
                                    <th>Người chỉnh sửa</th>
                                    <th>Thời gian sửa</th>
                                    <th>Id sản phẩm</th>
                                    <th>Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($ma_khuyen_mai as $key=>$value){
                                    $css_trang_thai = $value->trang_thai ? "badge-info": "badge-danger";
                                    $text_trang_thai = $value->trang_thai ? "Mở" : "Khóa";

                                    ?>
                                    <tr>
                                        <td><?php echo $value->ten_ma_khuyen_mai; ?></td>
                                        <td><span class="badge badge-pill <?php echo $css_trang_thai; ?>"><?php echo $text_trang_thai; ?></span></td>
                                        <td><?php echo $value->nguoi_dang; ?></td>
                                        <td><?php echo $value->thoi_gian_dang; ?></td>
                                        <td><?php echo $value->nguoi_chinh_sua; ?></td>
                                        <td><?php echo $value->thoi_gian_sua; ?></td>
                                        <td><?php echo $value->id_san_pham; ?></td>
                                        <td><button type="button" class="btn btn-cyan btn-sm" onclick="window.location.href='edit_ma_khuyen_mai.php?id=<?php echo $value->id;?>'">Edit</button>
                                            <button type="button" name="delete" class="btn btn-danger btn-sm" style="margin-top: 10px" onclick="window.location.href='delete_ma_khuyen_mai.php?id=<?php echo $value->id;?>' " >Delete</button></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>