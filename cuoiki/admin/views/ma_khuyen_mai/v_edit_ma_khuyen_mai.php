
<div class="page-wrapper">

    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <form class="form-horizontal" enctype="multipart/form-data" method="POST">
                        <div class="card-body">
                            <h4 class="card-title">Sửa mã khuyến mại<menu></menu></h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên mã khuyến mại</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="ten_ma_khuyen_mai" name="ten_ma_khuyen_mai"  value="<?php echo $ma_khuyen_mai_detail->ten_ma_khuyen_mai;?>" placeholder="Tên sản phẩm">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                                <div class="col-sm-9">
                                    <?php
                                    $text_trang_thai = $ma_khuyen_mai_detail->trang_thai ? "Mở" : "Khóa"; ?>
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="trang_thai" id="trang_thai">
                                        <option value="<?php echo $ma_khuyen_mai_detail->trang_thai;?>"><?php echo $text_trang_thai;?></option>
                                        <option value="1">Mở</option>
                                        <option value="0">Đóng</option>

                                    </select>
                                    </select>
                                </div>
                            </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Người đăng</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="nguoi_dang" name="nguoi_dang"  value="<?php echo $ma_khuyen_mai_detail->nguoi_dang;?>" placeholder="Người đăng">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Thời gian đăng</label>
                                    <div class="col-sm-9">
                                        <input type="datetime" class="form-control" id="thoi_gian_dang" name="thoi_gian_dang" value="<?php echo $ma_khuyen_mai_detail->thoi_gian_dang;?>" placeholder="Thời gian đăng">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Người chinh sửa</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="nguoi_chinh_sua" name="nguoi_chinh_sua" value="<?php echo $ma_khuyen_mai_detail->nguoi_chinh_sua;?>" placeholder="Người chinh sửa">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Thời gian chinh sửa</label>
                                    <div class="col-sm-9">
                                        <input type="datetime-local" class="form-control" id="thoi_gian_sua" name="thoi_gian_sua"  value="<?php echo $ma_khuyen_mai_detail->thoi_gian_chinh_sua;?>"  placeholder="Thời gian chỉnh sửa">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Id sản phẩm</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="id_san_pham" name="id_san_pham" value="<?php echo $ma_khuyen_mai_detail->id_san_pham;?>" placeholder="Id sản phẩm">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">

                                <!--                            <form  action="../c_ma_khuyen_mai.php" method="POST">-->


                                <button type="submit" name="btn_submit" class="btn btn-primary">Sửa</button>

                                <!--                            </form>-->

                                <!--                            <button type="submit" class="btn btn-primary">Submit</button>-->
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- editor -->

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
