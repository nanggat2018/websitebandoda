
<div class="page-wrapper">

    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <form class="form-horizontal" enctype="multipart/form-data" method="POST">
                        <div class="card-body">
                            <h4 class="card-title">Thêm người quản trị</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên người quản trị</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="ten_nguoi_quan_tri" name="ten_nguoi_quan_tri" placeholder="người quản trị">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Trạng thái</label>
                                <div class="col-sm-9">
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="trang_thai" id="trang_thai">
                                        <option>---Chọn---</option>
                                        <option value="1">Mở</option>
                                        <option value="0">Khóa</option>
                                    </select>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Tên đăng nhập</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="ten_dang_nhap" name="ten_dang_nhap" placeholder="Tên đăng nhập">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Mật khẩu</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="mat_khau" name="mat_khau" placeholder="Mật khẩu">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Địa chỉ</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="dia_chi" name="dia_chi" placeholder="Địa chỉ">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Số điện thoại</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="so_dien_thoai" name="so_dien_thoai" placeholder="Số điện thoại">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Ngày sinh</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="ngay_sinh" name="ngay_sinh" placeholder="Ngày sinh">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Id quyền</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="id_quyen" name="id_quyen" placeholder="Id quyền">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">

                                <!--                            <form  action="../c_ma_khuyen_mai.php" method="POST">-->


                                <button type="submit" name="btn_submit" class="btn btn-primary">Submit</button>

                                <!--                            </form>-->

                                <!--                            <button type="submit" class="btn btn-primary">Submit</button>-->
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- editor -->

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
