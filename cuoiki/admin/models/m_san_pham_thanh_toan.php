<?php
require_once ("database.php");
class m_san_pham_thanh_toan extends database{
    public function insert_san_pham_thanh_toan($id,$trang_thai,$id_san_pham,$gia_tien,$id_hoa_don){
            $sql = "INSERT INTO san_pham_thanh_toan VALUES (?,?,?,?,?)";
            $this->setQuery($sql);
            return $this->execute(array($id,$trang_thai,$id_san_pham,$gia_tien,$id_hoa_don));
    }
    public function read_san_pham_thanh_toan(){
        $sql = "select * from san_pham_thanh_toan";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_san_pham_thanh_toan_by_id_san_pham_thanh_toan($id){
        $sql = "select * from san_pham_thanh_toan where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function edit_san_pham_thanh_toan($id,$trang_thai,$id_san_pham,$gia_tien,$id_hoa_don){
    $sql = "update san_pham_thanh_toan set trang_thai = ?,id_san_pham = ?,gia_tien = ?,id_hoa_don = ? where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($trang_thai,$id_san_pham,$gia_tien,$id_hoa_don,$id));
    }
    public function delete_san_pham_thanh_toan($id){
        $sql = "delete from san_pham_thanh_toan where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}
