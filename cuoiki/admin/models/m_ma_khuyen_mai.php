<?php
require_once ("database.php");
class m_ma_khuyen_mai extends database{
    public function insert_ma_khuyen_mai($id,$ten_ma_khuyen_mai,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua,$id_san_pham){
        $sql = "insert into ma_khuyen_mai value (?,?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$ten_ma_khuyen_mai,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua,$id_san_pham));

    }
    public function read_ma_khuyen_mai(){
        $sql = "select * from ma_khuyen_mai";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_ma_khuyen_mai_by_id_ma_khuyen_mai($id){
        $sql = "select * from ma_khuyen_mai where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function edit_ma_khuyen_mai($id,$ten_ma_khuyen_mai,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua,$id_san_pham){
        $sql = "update ma_khuyen_mai set ten_ma_khuyen_mai = ?,trang_thai = ?,nguoi_dang = ?,thoi_gian_dang = ?,nguoi_chinh_sua = ?,thoi_gian_sua = ?,id_san_pham=? where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($ten_ma_khuyen_mai,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua,$id_san_pham,$id));
    }

    public function delete_ma_khuyen_mai($id){

        $sql = "delete from ma_khuyen_mai where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }

}