<?php
require_once ("database.php");
class m_nguoi_quan_tri extends database{

    public function insert_nguoi_quan_tri($id,$ten_nguoi_quan_tri,$trang_thai,$ten_dang_nhap,$mat_khau,$dia_chi,$so_dien_thoai,$email,$ngay_sinh,$id_quyen){
        $sql = "insert into nguoi_quan_tri value (?,?,?,?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$ten_nguoi_quan_tri,$trang_thai,$ten_dang_nhap,$mat_khau,$dia_chi,$so_dien_thoai,$email,$ngay_sinh,$id_quyen));
    }

    public function read_nguoi_quan_tri(){
        $sql = "select * from nguoi_quan_tri";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function read_nguoi_quan_tri_by_id_nguoi_quan_tri($id){
        $sql = "select * from nguoi_quan_tri where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }

    public function edit_nguoi_quan_tri($id,$ten_nguoi_quan_tri,$trang_thai,$ten_dang_nhap,$mat_khau,$dia_chi,$so_dien_thoai,$email,$ngay_sinh,$id_quyen){
        $sql = "update nguoi_quan_tri set ten_nguoi_quan_tri = ?,trang_thai = ?,ten_dang_nhap=?,mat_khau = ?,dia_chi = ?, so_dien_thoai = ?, email = ?, ngay_sinh = ?, id_quyen = ? where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($ten_nguoi_quan_tri,$trang_thai,$ten_dang_nhap,$mat_khau,$dia_chi,$so_dien_thoai,$email,$ngay_sinh,$id_quyen,$id));
    }

    public function delete_nguoi_quan_tri($id){
        $sql = "delete from nguoi_quan_tri where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }

}