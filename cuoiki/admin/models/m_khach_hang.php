<?php
require_once ("database.php");
class m_khach_hang extends database{

    public function read_khach_hang(){
        $sql = "select * from khach_hang";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_khach_hang_by_id_khach_hang($id){
        $sql = "select * from khach_hang where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function edit_khach_hang($id,$ten_khach_hang,$trang_thai,$ngay_sinh, $dia_chi, $so_dien_thoai, $email){
        $sql = "update khach_hang set ten_khach_hang = ?, trang_thai = ?,ngay_sinh = ?,dia_chi=?, so_dien_thoai=?, email=? where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($ten_khach_hang,$trang_thai,$ngay_sinh, $dia_chi, $so_dien_thoai, $email,$id));
    }

    public function delete_khach_hang($id){

        $sql = "delete from khach_hang where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }

}