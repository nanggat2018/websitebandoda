<?php
require_once ("database.php");
class m_danh_muc_sp extends database{
    public function insert_danh_muc_sp($id,$ten_danh_muc,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua){
        $sql = "insert into danh_muc_san_pham value (?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id,$ten_danh_muc,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua));

    }
    public function read_danh_muc_sp(){
        $sql = "select * from danh_muc_san_pham";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_danh_muc_sp_by_id_danh_muc($id){
        $sql = "select * from danh_muc_san_pham where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function edit_danh_muc_sp($id,$ten_danh_muc,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua){
        $sql = "update danh_muc_san_pham set ten_danh_muc = ?,trang_thai = ?,nguoi_dang = ?,thoi_gian_dang=?,nguoi_chinh_sua = ?,thoi_gian_sua = ? where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($ten_danh_muc,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua,$id));
    }
    public function delete_danh_muc_sp($id){

        $sql = "delete from danh_muc_san_pham where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }

}