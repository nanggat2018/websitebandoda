<?php
require_once ("database.php");
class m_hoa_don extends database{
//    public function insert_hoa_don($id,$ten_danh_muc,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_chinh_sua){
//        $sql = "insert into danh_muc_san_pham value (?,?,?,?,?,?,?)";
//        $this->setQuery($sql);
//        return $this->execute(array($id,$ten_danh_muc,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_chinh_sua));
//
//    }
    public function read_hoa_don(){
        $sql = "select * from hoa_don";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_hoa_don_by_id_hoa_don($id){
        $sql = "select * from hoa_don where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function edit_hoa_don($id,$trang_thai,$so_luong,$id_khach_hang){
        $sql = "update hoa_don set trang_thai = ?,so_luong = ?,id_khach_hang=? where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($trang_thai,$so_luong,$id_khach_hang,$id));
    }

    public function delete_hoa_don($id){

        $sql = "delete from hoa_don where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }

}