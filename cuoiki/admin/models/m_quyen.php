<?php
require_once ("database.php");
class m_quyen extends database{
    public function insert_quyen($id,$ten_quyen,$trang_thai){
            $sql = "insert into quyen value (?,?,?)";
            $this->setQuery($sql);
            return $this->execute(array($id,$ten_quyen,$trang_thai));
    }
    public function read_quyen(){
        $sql = "select * from quyen";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function read_quyen_by_id_quyen($id){
        $sql = "select * from quyen where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function edit_quyen($id,$ten_quyen,$trang_thai){
    $sql = "update quyen set ten_quyen = ?,trang_thai = ? where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($ten_quyen,$trang_thai,$id));
    }
    public function delete_quyen($id){
        $sql = "delete from quyen where id = ?";
        $this->setQuery($sql);
        return $this->execute(array($id));
    }
}


