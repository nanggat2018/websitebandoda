<?php
include ("models/m_san_pham_thanh_toan.php");
class c_san_pham_thanh_toan{
    public function show_san_pham_thanh_toan()
    {
        $m_san_pham_thanh_toan = new m_san_pham_thanh_toan();
        $san_pham_thanh_toans = $m_san_pham_thanh_toan->read_san_pham_thanh_toan();

        $view = "views/san_pham_thanh_toan/v_san_pham_thanh_toan.php";
        include("templates/layout.php");

    }
    public function add_san_pham_thanh_toan(){

        if(isset($_POST['btn_submit'])){
//            echo print_r($_POST);
//            die();

            $id = NULL;
            $trang_thai = $_POST['trang_thai'];
            $id_san_pham = $_POST['id_san_pham'];
            $gia_tien = $_POST['gia_tien'];
            $id_hoa_don = $_POST['id_hoa_don'];
            }


        $view = "views/san_pham_thanh_toan/v_add_san_pham_thanh_toan.php";
        include ("templates/layout.php");
    }
    public function edit_san_pham_thanh_toan()
    {
        $m_san_pham_thanh_toan = new m_san_pham_thanh_toan();
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $san_pham_thanh_toan_detail = $m_san_pham_thanh_toan->read_san_pham_thanh_toan_by_id_san_pham_thanh_toan($id);
            if (isset($_POST['btn-submit'])) {
                // $id = NULL;
                $trang_thai = $_POST['trang_thai'];
            $id_san_pham = $_POST['id_san_pham'];
            $gia_tien = $_POST['gia_tien'];
            $id_hoa_don = $_POST['id_hoa_don'];

            $san_pham_thanh_toan = new m_san_pham_thanh_toan();
            $result = $san_pham_thanh_toan->edit_san_pham_thanh_toan($id,$trang_thai,$id_san_pham,$gia_tien,$id_hoa_don);
            if($result )
            {
                echo "<script>alert('Sửa thành công');window.location='san_pham_thanh_toan.php'</script>";

            }
        }
            $view = "views/san_pham_thanh_toan/v_edit_san_pham_thanh_toan.php";
            include("templates/layout.php");
        }
    }
    function  delete_san_pham_thanh_toan()
    {
        if(isset($_GET["id"]))
        {
            $id = $_GET["id"];
//            $m_san_pham_thanh_toan = new m_san_pham_thanh_toan();
//            $kq = $m_san_pham_thanh_toan->delete_san_pham_thanh_toan($id);
            $san_pham_thanh_toan = new m_san_pham_thanh_toan();
            $result = $san_pham_thanh_toan->delete_san_pham_thanh_toan($id);
            if($result)
            {
                echo "<script>alert('Xóa thành công');window.location='san_pham_thanh_toan.php'</script>";
                
            }
        }
    }
}
?>