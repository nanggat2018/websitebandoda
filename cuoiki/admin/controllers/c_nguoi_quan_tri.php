<?php
include ("models/m_nguoi_quan_tri.php");
class c_nguoi_quan_tri
{
    public function show_nguoi_quan_tri()
    {
        $m_nguoi_quan_tri = new m_nguoi_quan_tri();
        $nguoi_quan_tri = $m_nguoi_quan_tri->read_nguoi_quan_tri();

        $view = "views/nguoi_quan_tri/v_nguoi_quan_tri.php";
        include("templates/layout.php");
    }

    public function add_nguoi_quan_tri(){

        if(isset($_POST['btn_submit'])){
//            echo print_r($_POST);
//            die();

            $id = NULL;
            $ten_nguoi_quan_tri= $_POST['ten_nguoi_quan_tri'];
            $trang_thai = $_POST['trang_thai'];
            $ten_dang_nhap = $_POST['ten_dang_nhap'];
            $mat_khau = $_POST['mat_khau'];
            $dia_chi = $_POST['dia_chi'];
            $so_dien_thoai = $_POST['so_dien_thoai'];
            $email = $_POST['email'];
            $ngay_sinh = $_POST['ngay_sinh'];
            $id_quyen = $_POST['id_quyen'];

            $nguoi_quan_tri = new m_nguoi_quan_tri();

            $result = $nguoi_quan_tri->insert_nguoi_quan_tri($id,$ten_nguoi_quan_tri,$trang_thai,$ten_dang_nhap,$mat_khau,$dia_chi,$so_dien_thoai,$email,$ngay_sinh,$id_quyen);
            if($result)
            {
                echo "<script>alert('Thêm thành công');window.location='nguoi_quan_tri.php'</script>";

            }

        }

        $view = "views/nguoi_quan_tri/v_add_nguoi_quan_tri.php";
        include ("templates/layout.php");
    }

    public function edit_nguoi_quan_tri()
    {
        $m_nguoi_quan_tri = new m_nguoi_quan_tri();
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $m_nguoi_quan_tri->read_nguoi_quan_tri_by_id_nguoi_quan_tri($id);

            $nguoi_quan_tri_detail = $m_nguoi_quan_tri->read_nguoi_quan_tri_by_id_nguoi_quan_tri($id);

            if (isset($_POST['btn_submit'])) {
                $ten_nguoi_quan_tri = $_POST['ten_nguoi_quan_tri'];
                $trang_thai = $_POST['trang_thai'];
                $ten_dang_nhap = $_POST['ten_dang_nhap'];
                $mat_khau = $_POST['mat_khau'];
                $dia_chi = $_POST['dia_chi'];
                $so_dien_thoai = $_POST['so_dien_thoai'];
                $email = $_POST['email'];
                $ngay_sinh = $_POST['ngay_sinh'];
                $id_quyen = $_POST['id_quyen'];

                $result = $m_nguoi_quan_tri->edit_nguoi_quan_tri($id,$ten_nguoi_quan_tri,$trang_thai,$ten_dang_nhap,$mat_khau,$dia_chi,$so_dien_thoai,$email,$ngay_sinh,$id_quyen);
//                if ($result) {
//                    if ($hinh_tieu_de != "") {
//                        move_uploaded_file($_FILES['f_hinh_anh']['tmp_name'], "public/image_san_pham/" . $hinh_tieu_de);
//                    }
//                    echo "<script>alert('thành công')</script>";
//                } else {
//                    echo "<script>alert('không thành công')</script>";
//
//                }

            }

        }
        $view = "views/nguoi_quan_tri/v_edit_nguoi_quan_tri.php";
        include("templates/layout.php");
    }

    function delete_nguoi_quan_tri()
    {
        if(isset($_GET["id"]))
        {
            $id= $_GET["id"];
            $m_nguoi_quan_tri= new m_nguoi_quan_tri();
            $kq = $m_nguoi_quan_tri->delete_nguoi_quan_tri($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='nguoi_quan_tri.php'</script>";

            }
        }
    }


}

?>



