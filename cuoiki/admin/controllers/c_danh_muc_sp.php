<?php
include ("models/m_danh_muc_sp.php");
class c_danh_muc_sp
{
    public function show_danh_muc_sp()
    {
        $m_danh_muc_sp = new m_danh_muc_sp();
        $danh_muc_sp = $m_danh_muc_sp->read_danh_muc_sp();

        $view = "views/danh_muc_sp/v_danh_muc_sp.php";
        include("templates/layout.php");

    }

    public function add_danh_muc_sp(){

        if(isset($_POST['btn_submit'])){
//            echo print_r($_POST);
//            die();

            $id = NULL;
            $ten_danh_muc= $_POST['ten_danh_muc'];
            $trang_thai = $_POST['trang_thai'];
            $nguoi_dang = $_POST['nguoi_dang'];
            $thoi_gian_dang = $_POST['thoi_gian_dang'];
            $nguoi_chinh_sua = NUll;
            $thoi_gian_sua = NULL;


            $danh_muc = new m_danh_muc_sp();
            $result = $danh_muc->insert_danh_muc_sp($id,$ten_danh_muc,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua);
            if($result)
            {
                echo "<script>alert('Thêm thành công');window.location='danh_muc_sp.php'</script>";

            }
//
        }

        $view = "views/danh_muc_sp/v_add_danh_muc_sp.php";
        include ("templates/layout.php");
    }

    public function edit_danh_muc_sp()
    {
        $m_danh_muc_sp = new m_danh_muc_sp();
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $m_danh_muc_sp->read_danh_muc_sp_by_id_danh_muc($id);

            $danh_muc_sp_detail = $m_danh_muc_sp->read_danh_muc_sp_by_id_danh_muc($id);

            if (isset($_POST['btn_submit'])) {
                $ten_danh_muc = $_POST['ten_danh_muc'];
                $trang_thai = $_POST['trang_thai'];
                $nguoi_dang = $_POST['nguoi_dang'];
                $thoi_gian_dang = $_POST['thoi_gian_dang'];
                $nguoi_chinh_sua = $_POST['nguoi_chinh_sua'];
                $thoi_gian_sua = $_POST['thoi_gian_sua'];
//
                $result = $m_danh_muc_sp->edit_danh_muc_sp($id,$ten_danh_muc,$trang_thai,$nguoi_dang,$thoi_gian_dang,$nguoi_chinh_sua,$thoi_gian_sua);

            }

        }
        $view = "views/danh_muc_sp/v_edit_danh_muc_sp.php";
        include("templates/layout.php");
    }

    function delete_danh_muc_sp()
    {
        if(isset($_GET["id"]))
        {
            $id= $_GET["id"];
            $m_danh_muc_sp= new m_danh_muc_sp();
            $kq = $m_danh_muc_sp->delete_danh_muc_sp($id);
            if($kq)
            {
                echo "<script>alert('Xóa thành công');window.location='danh_muc_sp.php'</script>";

            }
        }
    }


}

?>



