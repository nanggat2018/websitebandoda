<?php
require_once('database.php');
class m_mua_hang extends database {

    function themKhachHang($id,$ten_khach_hang,$trang_thai,$ngay_sinh,$dia_chi,$so_dien_thoai,$email,$hash)
    {
        $sql = "insert into khach_hang value (?,?,?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id, $ten_khach_hang, $trang_thai, $ngay_sinh, $dia_chi, $so_dien_thoai, $email,$hash));
    }

    function layKhachHang($hash)
    {
        $sql = "select * from khach_hang where hash = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($hash));
    }

    function delete_hash_khach($hash)
    {
        $sql = "update khach_hang set hash = null where hash = ?";
        $this->setQuery($sql);
        return $this->execute(array($hash));
    }


    function themHoaDon($id, $trang_thai, $tong_tien, $id_khach_hang,$hash2)
    {
        $sql = "insert into hoa_don value (?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id, $trang_thai, $tong_tien, $id_khach_hang,$hash2));
    }

    function layHoaDon($hash2)
    {
        $sql = "select * from hoa_don where hash2 = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($hash2));
    }

    function delete_hash_hoa_don($hash2)
    {
        $sql = "update hoa_don set hash2 = null where hash2 = ?";
        $this->setQuery($sql);
        return $this->execute(array($hash2));
    }

    function themSanPhamThanhToan($id, $trang_thai, $id_san_pham, $so_luong, $gia_tien, $id_hoa_don)
    {
        $sql = "insert into san_pham_thanh_toan value (?,?,?,?,?,?)";
        $this->setQuery($sql);
        return $this->execute(array($id, $trang_thai, $id_san_pham, $so_luong, $gia_tien, $id_hoa_don));
    }



}
?>