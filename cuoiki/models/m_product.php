<?php
require_once ("database.php");
class m_product extends database
{

    public function lay_san_pham_cho_gio_hang($chuoi)
    {
        $query = "select * from  san_pham where  ma_san_pham in ($chuoi)";
        $this->setQuery($query);
        return $this->loadAllRows();
    }

    public function read_product(){
        $sql = "select * from san_pham";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function read_cart_product_by_id_product($id){
        $sql = "select * from san_pham where id = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }

    public function read_product_by_id_product($id){
        $sql = "select * from san_pham where id = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }


    public function new_product(){
//        $sql = "select * from san_pham where thoi_gian_dang = current_date";

        $sql = " SELECT * FROM san_pham WHERE thoi_gian_dang ORDER BY thoi_gian_dang DESC LIMIT 2";

//        $sql = "SELECT TOP 2 * FROM san_pham WHERE thoi_gian_dang < current_timestamp ";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function wallet_product(){
        $sql = "select * from san_pham where id_danh_muc_san_pham =1";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function bag_product(){
        $sql = "select * from san_pham where id_danh_muc_san_pham =2 and trang_thai !=0";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function travel_product(){
        $sql = "select * from san_pham where id_danh_muc_san_pham =3";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function shoulder_product(){
        $sql = "select * from san_pham where id_danh_muc_san_pham =4";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }
    public function typical_product(){
        $sql = "select * from san_pham where trang_thai =1";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function read_product_category(){
        $sql = "select * from danh_muc_san_pham";
        $this->setQuery($sql);
        return $this->loadAllRows();
    }

    public function read_product_category_by_id($id_danh_muc_san_pham){
        $sql = "select * from san_pham where id_danh_muc_san_pham = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id_danh_muc_san_pham));
    }

}
?>
