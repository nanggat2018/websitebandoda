<?php
require_once ("database.php");
class m_cart extends database
{

    public function read_cart_product_by_id_product($id){
        $sql = "select * from san_pham where id = ?";
        $this->setQuery($sql);
        return $this->loadAllRows(array($id));
    }



    public function cart_delete($ten_san_pham){
        $sql = "delete from san_pham where ten_san_pham = ?";
        $this->setQuery($sql);
        return $this->execute(array($ten_san_pham));
    }

//    public function read_product_by_id_product($id){
//        $sql = "select * from san_pham where id = ?";
//        $this->setQuery($sql);
//        return $this->loadRow(array($id));
//    }


}
?>
