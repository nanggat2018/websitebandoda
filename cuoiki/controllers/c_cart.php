<?php
@session_start();
include ("models/m_cart.php");
class c_cart
{
    public function add_cart()
    {
        $m_cart = new m_cart();
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
//            $cart = $m_cart->read_product_by_id_product($id);

            $cart_product = $m_cart->read_cart_product_by_id_product($id);
//            $_SESSION['cart']['id'] = $cart_product;

            if (empty($_SESSION['cart']) || !array_key_exists($id, $_SESSION['cart'])) {
                $cart_product['so_luong'] = 1;
                $_SESSION['cart'][$id] = $cart_product;
            } else {
                $cart_product['so_luong'] = $_SESSION['cart'][$id]['so_luong'] + 1;
                $_SESSION['cart'][$id] = $cart_product;

            }

        }
    }

    public function cart()
    {
//        echo "<pre>";
//        foreach ($_SESSION['cart'] as $key=>$value){
//            var_dump($value[0]);
//        }
//        echo "</pre>";

        $all_product_cart = $_SESSION['cart'];

        $view = "views/cart/v_cart.php";
        include("templates/front-end/layout.php");
    }

    public function delete_cart()
    {

        unset($_SESSION['cart']);
        //unset($_SESSION['cart']);

//            if($kq)
//            {
//                echo "<script>alert('Xóa thành công');window.location='cart.php'</script>";
//
//            }

    }

    public function delete_sp()
    {
        if (isset($_GET['id'])) {
//            echo "123";

            $id = $_GET["id"];
            unset($_SESSION['cart'][$id]);

//            foreach ($_SESSION['cart'] as $key=>$value){
//                if($value[0]->id = $id){
//                    unset($_SESSION['cart'][$key]);
//                }
//            }
        }

    }

//    public function edit_cart()
//    {
//        if (isset($_GET['id'])) {
////            echo "123";
//            $id = $_GET["id"];
//            $_SESSION['cart']['thanh_tien'] = $_SESSION['cart']['so_luong'] * $_SESSION['cart']['gia_tien'];
////
//
////        }
//        }
//
//    }

    function capNhatGioHang($id, $so_luong, $gia_tien) {
        if(!is_numeric($so_luong))
            return false;
        $so_luong = (int)$so_luong;
        if($so_luong>0) {
//            $_SESSION['thanh_tien']-=$_SESSION['cart'][$id]*$gia_tien;
            $_SESSION['thanh_tien']=$so_luong*$gia_tien;

            $_SESSION['cart'][$id]=$so_luong;
            $_SESSION['so_luong']-=$_SESSION['cart'][$id];
            return false;
        }
        if($so_luong ==0)
            $this->delete_sp();
        return false;
    }

    function layGioHang()
    {
        if(isset($_SESSION['cart']))
            return $_SESSION['cart'];
        else
            return false;
    }

    function update()
    {

        foreach ($_POST['so_luong'] as $id=> $so_luong){
            if($so_luong < 0 || !is_numeric($so_luong)){
                continue;
            }
            if($so_luong == 0){
                unset($_SESSION['cart'][$id]);
            }
            else{
                $_SESSION['cart'][$id]['so_luong'] = $so_luong;
            }
        }
        header("location:cart.php");

    }

}


?>







