<div class="sub-menu-area tobo">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sub-menu">
                    <nav class="sub-menu-list">
                        <ul class="item">
                            <li><a href="#">Trang chủ<span class="shape-right"></span></a></li>
                            <li><a href="#">Sản phẩm<span class="shape-right"></span></a></li>
                            <li><a href="#">Giỏ hàng</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End sub-menu Area -->
<!-- wishlist content section start -->
<div class="shopping-cart-area nr-wish-area again-shop">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="s-cart-all">
                    <div class="cart-form table-responsive">
                        <form action="update.php" method="POST" style="text-align: center;">

                            <?php
                                if(!empty($all_product_cart) && isset($all_product_cart)):
                            ?>
                        <table id="shopping-cart-table" class="data-table cart-table" method="post">
                            <tr>
                                <th style="display: none">ID</th>
                                <th>Tên sản phẩm</th>
                                <th>Giá sản phẩm</th>
                                <th>Số lượng</th>
                                <th>Thành tiền</th>
                                <th>Hành động</th>

                            <?php
                            $tong_tien = 0;
                            foreach ($all_product_cart as $key=>$value) {?>
                                <tr>
                                    <td style="display: none"><?php echo $value[0]->id;?></td>
                                    <td><?php echo $value[0]->ten_san_pham;?></td>
                                    <td><?php echo $value[0]->gia_tien;?></td>
                                    <td type="text"><input style="padding: 5px; " type="number" name="so_luong[<?php echo $value[0]->id;?>]" value="<?php echo $value['so_luong'];?>"></td>
                                    <td><?php echo $thanh_tien = number_format($value['so_luong'] * $value[0]->gia_tien)?></td>
                                    <?php  $tong_tien = $tong_tien + $value['so_luong']*$value[0]->gia_tien ?>

                                    <td>
                                        <button style="background: skyblue; margin-right: 10px;" class="btn btn-cyan btn-sm">Cập nhật</button>
                                        <button type="button" id="btn_delete" name="delete_sp" class="btn btn-danger btn-sm" onclick="window.location.href='delete_sp.php?id=<?php echo $value[0]->id;?>' " >Delete</button>
                                    </td>

                                </tr>
                            <?php
                            }
                            ?>

                            <tfoot>

                            </tfoot>
                        </table>
                                    <div>Tổng tiền: <?php echo number_format($tong_tien) ?></div>

                            <?php
                                else:
                                    echo "Giỏ hàng rỗng";
                                endif;
                            ?>
                            <div id="th_giohang">

                            </div>


                    </div>
                    <div align="center">

                        <input type="button"  class="btn-primary"
                               onclick="window.location.href = 'mua_hang.php'"
                               value="Đặt hàng" />

                        <input type="button"  class="btn-success" id="btnxoagio"
                               value="Xóa giỏ hàng" class="art-button" />
                    </div>

                 </div>
                </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
