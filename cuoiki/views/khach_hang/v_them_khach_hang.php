<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6" >
            <form name="khach_hang" action="" method="POST">

                <div class="card">
                    <div class="card-body" align="center">
                        <h5 class="card-title">Hóa đơn</h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                <tr style="text-align: center">
                                    <th>Id sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>Giá tiền</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                $tong_tien = 0;
                                foreach ($_SESSION['cart'] as $key=>$value){
                                    ?>
                                    <tr>
                                        <td><?php echo $value[0]->id; ?></td>
                                        <td><?php echo $value['so_luong'] ?></td>
                                        <td><?php echo number_format($value['so_luong'] * $value[0]->gia_tien)?></td>
                                        <?php  $tong_tien = $tong_tien + $value['so_luong']*$value[0]->gia_tien ?>
                                        <!--                                    <td>--><?php //echo $_SESSION['cart'] ?><!--</td>-->
                                    </tr>

                                <?php }?>
                                <tr>
                                    <th colspan="4" style="text-align: center">Tổng tiền</th>
                                </tr>
                                <tr>
                                    <td id="tong_tien" colspan="4" style="text-align: center" name="tong_tien"><?php echo $tong_tien; ?></td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
<!--            </div>-->

<!--            <div class="col-md-6" style="text-align: center">-->
                <div class="card">
<!--                    <form class="form-horizontal" enctype="multipart/form-data" method="POST">-->
                        <div class="card-body" align="center">
                            <h5 class="card-title">Nhập thông tin khách hàng</h5>
<!--                            <form name="khach_hang" action="hoa_don.php" method="post">-->
                                <table align="center" width="90%" cellpadding="2px" cellspacing="0">

                                    <tr>
                                        <td align="center">
                                            Tên khách hàng<br />
                                            <input type="text" value="" name="ten_khach_hang" id="ten_khach_hang" style="width:450px; text-align:center"/>
                                    </tr>



                                    <tr style="display: none;">
                                        <td align="center">
                                            Trạng thái<br />
                                        <select class="select2 form-control custom-select" style="width:450px; text-align:center" name="trang_thai" id="trang_thai">
                                            <option>---Chọn---</option>
                                            <option value="1">Mở</option>
                                            <option value="0">Đóng</option>

                                        </select>
                                    </tr>

                                    <tr>
                                        <td align="center">Ngày Sinh<br /><input type="date" name="ngay_sinh" id="ngay_sinh" class="ngay" style="width:450px; text-align:center"/></td>
                                    </tr>
                                    <tr>
                                        <td align="center">Địa chỉ<br /><input type="text" value="" name="dia_chi" id="dia_chi" style="width:450px; text-align:center"/></td>
                                    </tr>
                                    <tr>
                                        <td align="center">Số điện thoại<br /><input type="text" name="so_dien_thoai" id="dien_thoai" style="width:450px; text-align:center"/></td>
                                    </tr>
                                    <tr>
                                        <td align="center">Email<br /><input type="text" value="" name="email" id="email" style="width:450px; text-align:center"/></td>
                                    </tr>


                                    <tr>
<!--                                        <td align="center"><input type="submit"  class="btn-danger" value="Lưu" name="submit" id="submit"/>-->
                                            <td align="center">
                                                <button class="btn-danger" type="submit" id="btn_gui_don" name="btn_gui_don"
                                                        onclick="window.location.href = 'mua_hang.php'">Gửi đơn hàng</button>
                                                <input type="button"  class="btn-primary"
                                                   onclick="window.location.href = 'cart.php'"
                                                   value="Quay về"/>
                                        </td>
                                    </tr>
                                </table>
                        </div>
                </div>
            </div>

            </form>
        </div>

        </div>
    </div>
