<div class="sub-menu-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sub-menu">
                    <nav class="sub-menu-list">
                        <ul class="item">
                            <li><a href="#">Trang chủ<span class="shape-right"></span></a></li>
                            <li><a href="#">Sản phẩm<span class="shape-right"></span></a></li>
                            <li><a href="#"><?php echo $product_detail->ten_san_pham; ?></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End sub-menu Area -->
<section>
    <div class="pro-detail-banner-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="banner-left-pro-tab">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active single-product-top" id="product1">
                                <img src="admin/public/image_san_pham/<?php echo $product_detail->hinh_anh; ?>" alt="" />
                                <img class="pro-back-img" src="admin/public/image_san_pham/<?php echo $product_detail->hinh_anh_hover;?>" alt="" />


                            </div>
                            <div class="tab-pane" id="product2">
                                <img src="public/layout/assets/images/banner/tab/1-2.webp" alt="" />
                            </div>
                            <div class="tab-pane" id="product3">
                                <img src="public/layout/assets/images/banner/tab/1-3.webp" alt="" />
                            </div>
                            <div class="tab-pane" id="product4">
                                <img src="public/layout/assets/images/product/women/lt.webp" alt="" />
                            </div>
                        </div>
                        <!-- Nav tabs -->
<!--                        <ul class="nav justify-content-center tab-position">-->
<!--                            <li class="active">-->
<!--                                <a href="#product1" data-bs-toggle="tab">-->
<!--                                    <div class="pro-detail-tab-menu">-->
<!--                                        <img src="public/layout/assets/images/banner/tab/1.webp" alt="" />-->
<!--                                    </div>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                        </ul>-->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-pro-list">
                        <a href="#"><?php echo $product_detail->ten_san_pham; ?></a>
                        <div class="pro-list-like">
                            <span class="star"><i class="fa fa-star"></i></span>
                            <span class="star"><i class="fa fa-star"></i></span>
                            <span class="star"><i class="fa fa-star"></i></span>
                            <span class="star"><i class="fa fa-star"></i></span>
                            <span class="star-change"><i class="fa fa-star"></i></span>
                        </div>
                        <div class="pro-list-price">
                            <h1><?php echo number_format($product_detail->gia_tien); ?>₫</h1>
                        </div>
                        <p class="border-btm"><?php echo $product_detail->mo_ta_chi_tiet; ?></p>
                        <ul class="available d-flex flex-wrap">
<!--                            <li>color :</li>-->
<!--                            <li class="one"><a href="#"></a></li>-->
<!--                            <li class="two"><a href="#"></a></li>-->
<!--                            <li class="three"><a href="#"></a></li>-->
<!--                            <li class="four"><a href="#">more</a></li>-->
<!--                            <li class="five">Size :</li>-->
<!--                            <li class="dropdown">-->
<!--                                <a class="dropdown-toggle size-search" data-bs-toggle="dropdown" href="#">s-->
<!--                                    <span class="caret"></span></a>-->
<!--                                <ul class="dropdown-menu sizing">-->
<!--                                    <li><a href="#">S</a></li>-->
<!--                                    <li><a href="#">M</a></li>-->
<!--                                    <li><a href="#">L</a></li>-->
<!--                                    <li><a href="#">XL</a></li>-->
<!--                                </ul>-->
<!--                            </li>-->
<!--                            <li><a href="#"></a></li>-->
                        </ul>
                        <ul class="pro-list-sop">
                            <li class="pro-cart"><a href="#">ADD TO CART</a></li>
                            <li><a href="#"><i class="fa fa-heart"></i></a></li>
                            <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                        </ul>

<!--                        <ul class="share d-flex flex-wrap">-->
<!--                            <li>Shared this: </li>-->
<!--                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>-->
<!--                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
<!--                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>-->
<!--                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
<!--                        </ul>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Start tab Area -->
<!--<div class="pro-detail-tab-area">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <div class="pro-detail-tab">-->
<!--                    Nav tabs -->
<!--                    <ul class="nav tab-menu-style">-->
<!--                        <li><a class="active" href="#description" data-bs-toggle="tab">Description</a></li>-->
<!--                        <li><a href="#reviews" data-bs-toggle="tab">Reviews</a></li>-->
<!--                        <li><a href="#shipping" data-bs-toggle="tab">Shipping</a></li>-->
<!--                    </ul>-->
<!--                    Tab panes -->
<!--                    <div class="tab-content">-->
<!--                        <div role="tabpanel" class="tab-pane active" id="description">-->
<!--                            <div class="pro-detail-tab-cont">-->
<!--                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet libero-->
<!--                                    nec felis ultricies facilisis. Maecenas ut elit at sem porttitor fringilla.-->
<!--                                    Sed sagittis enim vitae lectus condimentum bibendum. Sed at suscipit eros,-->
<!--                                    ac sodales justo. Nullam vitae metus eget massa sollicitudin gravida. Sed-->
<!--                                    mauris lectus, tempus vel turpis quis, congue vestibulum libero. Vivamus-->
<!--                                    ullamcorper accumsan fermentum. In hac habitasse platea dictumst. In-->
<!--                                    vulputate enim dui, eu molestie risus placerat at. Praesent nec magna sed-->
<!--                                    nisi sagittis mollis ut at augue. Sed molestie eros et nisl bibendum luctus.-->
<!--                                    Vestibulum eu lectus a lectus viverra luctus nec et leo.</p>-->
<!--                                <p>In hac habitasse platea dictumst. In vulputate enim dui, eu molestie risus-->
<!--                                    placerat at. Praesent nec magna sed nisi sagittis mollis ut at augue. Sed-->
<!--                                    molestie eros et nisl bibendum luctus. Vestibulum eu lectus a lectus viverra-->
<!--                                    luctus nec et leo.</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div role="tabpanel" class="tab-pane" id="reviews">-->
<!--                            <div class="product-tab-desc">-->
<!--                                <div class="product-page-comments">-->
<!--                                    <h2>1 review for Integer consequat ante lectus</h2>-->
<!--                                    <ul>-->
<!--                                        <li>-->
<!--                                            <div class="product-comments">-->
<!--                                                <img src="public/layout/assets/images/logo/logo.webp" alt="">-->
<!--                                                <div class="product-comments-content">-->
<!--                                                    <p><strong>admin</strong> --->
<!--                                                        <span>March 7, 2015:</span>-->
<!--                                                        <span class="pro-comments-rating">-->
<!--                                                                    <i class="fa fa-star"></i>-->
<!--                                                                    <i class="fa fa-star"></i>-->
<!--                                                                    <i class="fa fa-star"></i>-->
<!--                                                                    <i class="fa fa-star"></i>-->
<!--                                                                </span>-->
<!--                                                    </p>-->
<!--                                                    <div class="desc">-->
<!--                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.-->
<!--                                                        Nam fringilla augue nec est tristique auctor. Donec non-->
<!--                                                        est at libero vulputate rutrum.-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </li>-->
<!--                                    </ul>-->
<!--                                    <div class="review-form-wrapper">-->
<!--                                        <h3>Add a review</h3>-->
<!--                                        <form action="#">-->
<!--                                            <input placeholder="your name" type="text">-->
<!--                                            <input placeholder="your email" type="email">-->
<!--                                            <div class="your-rating">-->
<!--                                                <h5>Your Rating</h5>-->
<!--                                                <span>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                        </span>-->
<!--                                                <span>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                        </span>-->
<!--                                                <span>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                        </span>-->
<!--                                                <span>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                            <a href="#"><i class="fa fa-star"></i></a>-->
<!--                                                        </span>-->
<!--                                            </div>-->
<!--                                            <textarea id="product-message" cols="30" rows="10"-->
<!--                                                      placeholder="Your Rating"></textarea>-->
<!--                                            <input value="submit" type="submit">-->
<!--                                        </form>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div role="tabpanel" class="tab-pane" id="shipping">-->
<!--                            <div class="pro-detail-tab-cont">-->
<!--                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet libero-->
<!--                                    nec felis ultricies facilisis. Maecenas ut elit at sem porttitor fringilla.-->
<!--                                    Sed sagittis enim vitae lectus condimentum bibendum. Sed at suscipit eros,-->
<!--                                    ac sodales justo. Nullam vitae metus eget massa sollicitudin gravida. Sed-->
<!--                                    mauris lectus, tempus vel turpis quis, congue vestibulum libero. Vivamus-->
<!--                                    ullamcorper accumsan fermentum. In hac habitasse platea dictumst. In-->
<!--                                    vulputate enim dui, eu eu lectus a lectus viverra luctus nec et leo.</p>-->
<!--                                <p>In hac habitasse platea dictumst. In vulputate enim dui, eu molestie risus-->
<!--                                    placerat at. Praesent nec magna sed nvulputate enim dui, eu molestie risus-->
<!--                                    placerat at. Praesent nec magna sed nisi sagittis mollis ut at augue. Sed-->
<!--                                    molestie eros et nisl bibendum luctus. Vestibulum eu lectus a lectus viverra-->
<!--                                    luctus nec et leo.</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--Start product owl  Area-1 -->
<!--<div class="product-detail-area">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <div class="product-heading-area tas">-->
<!--                    <div class="product-heading">-->
<!--                        <h2>related products //</h2>-->
<!--                    </div>-->
<!--                </div>-->
<!--               start single-project -->
<!--                <div class="row">-->
<!--                    <div class="pro-detail-pro-total">-->
<!--                        <div class="col-md-3 col-sm-3">-->
<!--                            <div class="single-product">-->
<!--                                <div class="single-product-top">-->
<!--                                    <a href="#">-->
<!--                                        <img class="pro-font-img" src="public/layout/assets/images/product/all/5.webp" alt="" />-->
<!--                                        <img class="pro-back-img" src="public/layout/assets/images/product/all/12.webp" alt="" />-->
<!--                                    </a>-->
<!--                                    <h3 class="pro-price">$95</h3>-->
<!--                                    <div class="pro-icon">-->
<!--                                        <ul>-->
<!--                                            <li class="heart-left"><a class="like" href="#"><i-->
<!--                                                        class="pe-7s-like"></i></a></li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a>-->
<!--                                            </li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a>-->
<!--                                            </li>-->
<!--                                        </ul>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="single-product-bootom">-->
<!--                                    <div class="pro-btm-text">-->
<!--                                        <a href="#">Smoothie Foundation</a>-->
<!--                                    </div>-->
<!--                                    <div class="pro-star">-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star-change"><i class="fa fa-star"-->
<!--                                                                     aria-hidden="true"></i></span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-md-3 col-sm-3">-->
<!--                            <div class="single-product">-->
<!--                                <div class="single-product-top">-->
<!--                                    <a href="#">-->
<!--                                        <img class="pro-font-img" src="public/layout/assets/images/product/all/6.webp" alt="" />-->
<!--                                        <img class="pro-back-img" src="public/layout/assets/images/product/all/11.webp" alt="" />-->
<!--                                    </a>-->
<!--                                    <h3 class="pro-price">$95</h3>-->
<!--                                    <div class="pro-icon">-->
<!--                                        <ul>-->
<!--                                            <li class="heart-left"><a class="like" href="#"><i-->
<!--                                                        class="pe-7s-like"></i></a></li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a>-->
<!--                                            </li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a>-->
<!--                                            </li>-->
<!--                                        </ul>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="single-product-bootom">-->
<!--                                    <div class="pro-btm-text">-->
<!--                                        <a href="#">Smoothie Foundation</a>-->
<!--                                    </div>-->
<!--                                    <div class="pro-star">-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star-change"><i class="fa fa-star"-->
<!--                                                                     aria-hidden="true"></i></span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-md-3 col-sm-3">-->
<!--                            <div class="single-product">-->
<!--                                <div class="single-product-top">-->
<!--                                    <a href="#">-->
<!--                                        <img class="pro-font-img" src="public/layout/assets/images/product/all/7.webp" alt="" />-->
<!--                                        <img class="pro-back-img" src="public/layout/assets/images/product/all/10.webp" alt="" />-->
<!--                                    </a>-->
<!--                                    <h3 class="pro-price">$95</h3>-->
<!--                                    <div class="pro-icon">-->
<!--                                        <ul>-->
<!--                                            <li class="heart-left"><a class="like" href="#"><i-->
<!--                                                        class="pe-7s-like"></i></a></li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a>-->
<!--                                            </li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a>-->
<!--                                            </li>-->
<!--                                        </ul>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="single-product-bootom">-->
<!--                                    <div class="pro-btm-text">-->
<!--                                        <a href="#">Smoothie Foundation</a>-->
<!--                                    </div>-->
<!--                                    <div class="pro-star">-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star-change"><i class="fa fa-star"-->
<!--                                                                     aria-hidden="true"></i></span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-md-3 col-sm-3">-->
<!--                            <div class="single-product">-->
<!--                                <div class="single-product-top">-->
<!--                                    <a href="#">-->
<!--                                        <img class="pro-font-img" src="public/layout/assets/images/product/all/8.webp" alt="" />-->
<!--                                        <img class="pro-back-img" src="public/layout/assets/images/product/all/9.webp" alt="" />-->
<!--                                    </a>-->
<!--                                    <h3 class="pro-price">$95</h3>-->
<!--                                    <div class="pro-icon">-->
<!--                                        <ul>-->
<!--                                            <li class="heart-left"><a class="like" href="#"><i-->
<!--                                                        class="pe-7s-like"></i></a></li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a>-->
<!--                                            </li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a>-->
<!--                                            </li>-->
<!--                                        </ul>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="single-product-bootom">-->
<!--                                    <div class="pro-btm-text">-->
<!--                                        <a href="#">Smoothie Foundation</a>-->
<!--                                    </div>-->
<!--                                    <div class="pro-star">-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star-change"><i class="fa fa-star"-->
<!--                                                                     aria-hidden="true"></i></span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-md-3 col-sm-3">-->
<!--                            <div class="single-product">-->
<!--                                <div class="single-product-top">-->
<!--                                    <a href="#">-->
<!--                                        <img class="pro-font-img" src="public/layout/assets/images/product/all/9.webp" alt="" />-->
<!--                                        <img class="pro-back-img" src="public/layout/assets/images/product/all/8.webp" alt="" />-->
<!--                                    </a>-->
<!--                                    <h3 class="pro-price">$95</h3>-->
<!--                                    <div class="pro-icon">-->
<!--                                        <ul>-->
<!--                                            <li class="heart-left"><a class="like" href="#"><i-->
<!--                                                        class="pe-7s-like"></i></a></li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a>-->
<!--                                            </li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a>-->
<!--                                            </li>-->
<!--                                        </ul>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="single-product-bootom">-->
<!--                                    <div class="pro-btm-text">-->
<!--                                        <a href="#">Smoothie Foundation</a>-->
<!--                                    </div>-->
<!--                                    <div class="pro-star">-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star-change"><i class="fa fa-star"-->
<!--                                                                     aria-hidden="true"></i></span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                End single-product -->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--End product owl  Area-1 -->
<!--Start product owl  Area-2 -->
<!--<div class="product-detail-owl-area leatest">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <div class="product-heading-area">-->
<!--                    <div class="product-heading">-->
<!--                        <h2>upsell products //</h2>-->
<!--                    </div>-->
<!--                </div>-->
<!--                start single-project -->
<!--                <div class="row">-->
<!--                    <div class="pro-detail-pro-total">-->
<!--                        <div class="col-md-3">-->
<!--                            <div class="single-product">-->
<!--                                <div class="single-product-top">-->
<!--                                    <a href="#">-->
<!--                                        <img class="pro-font-img" src="public/layout/assets/images/product/all/9.webp" alt="" />-->
<!--                                        <img class="pro-back-img" src="public/layout/assets/images/product/all/8.webp" alt="" />-->
<!--                                    </a>-->
<!--                                    <h3 class="pro-price">$>95</h3>-->
<!--                                    <div class="pro-icon">-->
<!--                                        <ul>-->
<!--                                            <li class="heart-left"><a class="like" href="#"><i-->
<!--                                                        class="pe-7s-like"></i></a></li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a>-->
<!--                                            </li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a>-->
<!--                                            </li>-->
<!--                                        </ul>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="single-product-bootom">-->
<!--                                    <div class="pro-btm-text">-->
<!--                                        <a href="#">Smoothie Foundation</a>-->
<!--                                    </div>-->
<!--                                    <div class="pro-star">-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star-change"><i class="fa fa-star"-->
<!--                                                                     aria-hidden="true"></i></span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-md-3">-->
<!--                            <div class="single-product">-->
<!--                                <div class="single-product-top">-->
<!--                                    <a href="#">-->
<!--                                        <img class="pro-font-img" src="public/layout/assets/images/product/all/10.webp" alt="" />-->
<!--                                        <img class="pro-back-img" src="public/layout/assets/images/product/all/7.webp" alt="" />-->
<!--                                    </a>-->
<!--                                    <h3 class="pro-price">$95</h3>-->
<!--                                    <div class="pro-icon">-->
<!--                                        <ul>-->
<!--                                            <li class="heart-left"><a class="like" href="#"><i-->
<!--                                                        class="pe-7s-like"></i></a></li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a>-->
<!--                                            </li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a>-->
<!--                                            </li>-->
<!--                                        </ul>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="single-product-bootom">-->
<!--                                    <div class="pro-btm-text">-->
<!--                                        <a href="#">Smoothie Foundation</a>-->
<!--                                    </div>-->
<!--                                    <div class="pro-star">-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star-change"><i class="fa fa-star"-->
<!--                                                                     aria-hidden="true"></i></span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-md-3">-->
<!--                            <div class="single-product">-->
<!--                                <div class="single-product-top">-->
<!--                                    <a href="#">-->
<!--                                        <img class="pro-font-img" src="public/layout/assets/images/product/all/11.webp" alt="" />-->
<!--                                        <img class="pro-back-img" src="public/layout/assets/images/product/all/6.webp" alt="" />-->
<!--                                    </a>-->
<!--                                    <h3 class="pro-price">$95</h3>-->
<!--                                    <div class="pro-icon">-->
<!--                                        <ul>-->
<!--                                            <li class="heart-left"><a class="like" href="#"><i-->
<!--                                                        class="pe-7s-like"></i></a></li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a>-->
<!--                                            </li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a>-->
<!--                                            </li>-->
<!--                                        </ul>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="single-product-bootom">-->
<!--                                    <div class="pro-btm-text">-->
<!--                                        <a href="#">Smoothie Foundation</a>-->
<!--                                    </div>-->
<!--                                    <div class="pro-star">-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star-change"><i class="fa fa-star"-->
<!--                                                                     aria-hidden="true"></i></span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-md-3">-->
<!--                            <div class="single-product">-->
<!--                                <div class="single-product-top">-->
<!--                                    <a href="#">-->
<!--                                        <img class="pro-font-img" src="public/layout/assets/images/product/all/12.webp" alt="" />-->
<!--                                        <img class="pro-back-img" src="public/layout/assets/images/product/all/5.webp" alt="" />-->
<!--                                    </a>-->
<!--                                    <h3 class="pro-price">$95</h3>-->
<!--                                    <div class="pro-icon">-->
<!--                                        <ul>-->
<!--                                            <li class="heart-left"><a class="like" href="#"><i-->
<!--                                                        class="pe-7s-like"></i></a></li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a>-->
<!--                                            </li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a>-->
<!--                                            </li>-->
<!--                                        </ul>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="single-product-bootom">-->
<!--                                    <div class="pro-btm-text">-->
<!--                                        <a href="#">Smoothie Foundation</a>-->
<!--                                    </div>-->
<!--                                    <div class="pro-star">-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star-change"><i class="fa fa-star"-->
<!--                                                                     aria-hidden="true"></i></span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-md-3">-->
<!--                            <div class="single-product">-->
<!--                                <div class="single-product-top">-->
<!--                                    <a href="#">-->
<!--                                        <img class="pro-font-img" src="public/layout/assets/images/product/all/13.webp" alt="" />-->
<!--                                        <img class="pro-back-img" src="public/layout/assets/images/product/all/4.webp" alt="" />-->
<!--                                    </a>-->
<!--                                    <h3 class="pro-price">$95</h3>-->
<!--                                    <div class="pro-icon">-->
<!--                                        <ul>-->
<!--                                            <li class="heart-left"><a class="like" href="#"><i-->
<!--                                                        class="pe-7s-like"></i></a></li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-cart"></i></a>-->
<!--                                            </li>-->
<!--                                            <li class="heart-left"><a href="#"><i class="pe-7s-search"></i></a>-->
<!--                                            </li>-->
<!--                                        </ul>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="single-product-bootom">-->
<!--                                    <div class="pro-btm-text">-->
<!--                                        <a href="#">Smoothie Foundation</a>-->
<!--                                    </div>-->
<!--                                    <div class="pro-star">-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star"><i class="fa fa-star" aria-hidden="true"></i></span>-->
<!--                                        <span class="star-change"><i class="fa fa-star"-->
<!--                                                                     aria-hidden="true"></i></span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                End single-product -->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->