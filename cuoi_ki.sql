-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 23, 2022 lúc 08:26 AM
-- Phiên bản máy phục vụ: 10.4.22-MariaDB
-- Phiên bản PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `cuoi_ki`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `danh_muc_san_pham`
--

CREATE TABLE `danh_muc_san_pham` (
  `id` int(11) NOT NULL,
  `ten_danh_muc` varchar(255) NOT NULL,
  `trang_thai` tinyint(3) NOT NULL DEFAULT 1,
  `nguoi_dang` varchar(40) NOT NULL,
  `thoi_gian_dang` datetime NOT NULL,
  `nguoi_chinh_sua` varchar(40) DEFAULT NULL,
  `thoi_gian_sua` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `danh_muc_san_pham`
--

INSERT INTO `danh_muc_san_pham` (`id`, `ten_danh_muc`, `trang_thai`, `nguoi_dang`, `thoi_gian_dang`, `nguoi_chinh_sua`, `thoi_gian_sua`) VALUES
(1, 'Ví da Hàn Quốc', 1, 'Lê Thị Hoa', '2022-01-01 11:02:09', NULL, NULL),
(2, 'Túi sách Việt Nam', 1, 'Lê Thị Hoa', '2022-01-01 11:03:01', NULL, NULL),
(3, 'Túi du lịch Nhật Bản', 1, 'Đỗ Nguyễn Bình', '2022-01-01 11:05:04', NULL, NULL),
(18, 'Túi da đeo vai Thái Lan', 1, 'Đỗ Nguyễn Bình', '2022-01-01 11:05:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoa_don`
--

CREATE TABLE `hoa_don` (
  `id` int(11) NOT NULL,
  `trang_thai` tinyint(3) NOT NULL DEFAULT 1,
  `tong_tien` int(11) NOT NULL,
  `id_khach_hang` int(11) NOT NULL,
  `hash2` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `hoa_don`
--

INSERT INTO `hoa_don` (`id`, `trang_thai`, `tong_tien`, `id_khach_hang`, `hash2`) VALUES
(46, 1, 8000000, 90, '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khach_hang`
--

CREATE TABLE `khach_hang` (
  `id` int(11) NOT NULL,
  `ten_khach_hang` varchar(40) NOT NULL,
  `trang_thai` tinyint(3) NOT NULL DEFAULT 1,
  `ngay_sinh` date NOT NULL,
  `dia_chi` varchar(255) NOT NULL,
  `so_dien_thoai` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `hash` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `khach_hang`
--

INSERT INTO `khach_hang` (`id`, `ten_khach_hang`, `trang_thai`, `ngay_sinh`, `dia_chi`, `so_dien_thoai`, `email`, `hash`) VALUES
(90, 'Nguyễn Huy Hoàng', 0, '2022-04-15', 'Phú Lương - Hà Đông - Hà Nội', '0977676856', 'lethihoa181299@gmail.com', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ma_khuyen_mai`
--

CREATE TABLE `ma_khuyen_mai` (
  `id` int(11) NOT NULL,
  `ten_ma_khuyen_mai` varchar(255) NOT NULL,
  `trang_thai` tinyint(3) NOT NULL DEFAULT 1,
  `nguoi_dang` varchar(40) NOT NULL,
  `thoi_gian_dang` datetime NOT NULL,
  `nguoi_chinh_sua` varchar(40) DEFAULT NULL,
  `thoi_gian_sua` datetime DEFAULT NULL,
  `id_san_pham` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `ma_khuyen_mai`
--

INSERT INTO `ma_khuyen_mai` (`id`, `ten_ma_khuyen_mai`, `trang_thai`, `nguoi_dang`, `thoi_gian_dang`, `nguoi_chinh_sua`, `thoi_gian_sua`, `id_san_pham`) VALUES
(2, 'Mã khuyến mại giảm 25%', 1, 'Đỗ Nguyễn Bình', '2022-04-07 02:53:34', '', '0000-00-00 00:00:00', 3),
(3, 'Mã khuyến mại 50%', 0, 'Hoa xinh', '2022-04-07 08:50:00', 'Hoa', '2022-04-07 09:09:00', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nguoi_quan_tri`
--

CREATE TABLE `nguoi_quan_tri` (
  `id` int(11) NOT NULL,
  `ten_nguoi_quan_tri` varchar(40) NOT NULL,
  `trang_thai` tinyint(3) NOT NULL DEFAULT 1,
  `ten_dang_nhap` varchar(255) NOT NULL,
  `mat_khau` varchar(40) NOT NULL,
  `dia_chi` varchar(255) NOT NULL,
  `so_dien_thoai` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `ngay_sinh` date NOT NULL,
  `id_quyen` tinyint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `nguoi_quan_tri`
--

INSERT INTO `nguoi_quan_tri` (`id`, `ten_nguoi_quan_tri`, `trang_thai`, `ten_dang_nhap`, `mat_khau`, `dia_chi`, `so_dien_thoai`, `email`, `ngay_sinh`, `id_quyen`) VALUES
(1, 'Lê Thị Hoa', 1, 'hoaxinh123', '123456', 'Khu đô thị Phú Lương - Hà Đông - Hà Nội', '0977626821', 'nanggat2018@gmail.com', '2000-04-10', 1),
(2, 'Đỗ Nguyễn Bình', 1, 'binh123', '123456', 'Khu đô thị Văn Phú - Hà Đông - Hà Nội', '0977626829', 'binh@gmail.com', '1999-04-10', 4),
(3, 'Lê Thanh Trúc', 1, 'truc123', '123456', 'Khu đô thị Phú Lương - Hà Đông - Hà Nội', '0977626821', 'truc@gmail.com', '2001-04-10', 2),
(4, 'Ngô Xuân Nam', 1, 'nam123', '123456', 'Khu đô thị Thanh Hà - Hà Đông - Hà Nội', '0977626821', 'nam@gmail.com', '1999-04-10', 5),
(5, 'Phạm Ngọc Anh', 1, 'ngoc123', '123456', 'Khu đô thị Kiến Hưng - Hà Đông - Hà Nội', '0977626821', 'anh@gmail.com', '2003-04-07', 3),
(7, 'Hoa xinh 12', 1, 'hoa123', '123456', 'Phú Lương - Hà Đông - Hà Nội', '0977676856', 'Test@gmail.com', '2000-04-10', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `quyen`
--

CREATE TABLE `quyen` (
  `id` int(11) NOT NULL,
  `ten_quyen` varchar(255) NOT NULL,
  `trang_thai` tinyint(3) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `quyen`
--

INSERT INTO `quyen` (`id`, `ten_quyen`, `trang_thai`) VALUES
(1, 'Quản trị', 1),
(2, 'Nhân viên', 1),
(3, 'Kế toán', 1),
(4, 'Trưởng phòng', 1),
(5, 'Phó phòng', 1),
(7, 'abc', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `san_pham`
--

CREATE TABLE `san_pham` (
  `id` int(11) NOT NULL,
  `ten_san_pham` varchar(255) NOT NULL,
  `trang_thai` tinyint(3) NOT NULL DEFAULT 1,
  `mo_ta_chi_tiet` text NOT NULL,
  `gia_tien` int(11) NOT NULL,
  `so_luong` int(11) NOT NULL,
  `hinh_anh` varchar(255) NOT NULL,
  `hinh_anh_hover` varchar(255) NOT NULL,
  `nguoi_dang` varchar(40) NOT NULL,
  `thoi_gian_dang` datetime NOT NULL,
  `nguoi_chinh_sua` varchar(40) DEFAULT NULL,
  `thoi_gian_sua` datetime DEFAULT NULL,
  `id_danh_muc_san_pham` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `san_pham`
--

INSERT INTO `san_pham` (`id`, `ten_san_pham`, `trang_thai`, `mo_ta_chi_tiet`, `gia_tien`, `so_luong`, `hinh_anh`, `hinh_anh_hover`, `nguoi_dang`, `thoi_gian_dang`, `nguoi_chinh_sua`, `thoi_gian_sua`, `id_danh_muc_san_pham`) VALUES
(1, 'Sản phẩm 1', 2, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 1000000, 20, '1.jpg', 'hover1.jpg', 'Lê Thị Hoa', '2001-01-22 16:39:00', '', '0000-00-00 00:00:00', 2),
(2, 'Sản phẩm 2', 1, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 2500000, 30, '2.jpg', 'hover2.jpg', 'Lê Thị Hoa', '2002-01-22 16:39:00', '', '0000-00-00 00:00:00', 2),
(3, 'Sản phẩm 3', 1, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 2500000, 25, '3.jpg', 'hover3.jpg', 'Lê Thị Hoa', '2003-01-22 16:39:00', '', '0000-00-00 00:00:00', 3),
(4, 'Sản phẩm 4', 1, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 3500000, 15, '4.jpg', 'hover4.jpg', 'Lê Thị Hoa', '2004-01-22 16:39:00', '', '0000-00-00 00:00:00', 4),
(5, 'Sản phẩm 5', 1, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 2500000, 20, '5.jpg', 'hover5.jpg', 'Lê Thị Hoa', '2005-01-22 16:39:00', '', '0000-00-00 00:00:00', 4),
(6, 'Sản phẩm 6', 1, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 1000000, 20, '6.jpg', 'hover6.jpg', 'Lê Thị Hoa', '2006-01-22 16:39:00', '', '0000-00-00 00:00:00', 4),
(7, 'Sản phẩm 7', 1, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 1000000, 20, '7.jpg', 'hover7.jpg', 'Lê Thị Hoa', '2007-01-22 16:39:00', '', '0000-00-00 00:00:00', 1),
(8, 'Sản phẩm 8', 2, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 1000000, 20, '8.jpg', 'hover8.jpg', 'Lê Thị Hoa', '2008-01-22 16:39:00', '', '0000-00-00 00:00:00', 1),
(9, 'Sản phẩm 9', 1, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 5000000, 20, '9.jpg', 'hover9.jpg', 'Đỗ Nguyễn Bình', '2009-01-22 16:39:00', '', '0000-00-00 00:00:00', 1),
(10, 'Sản phẩm 10', 1, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 8000000, 20, '10.jpg', 'hover10.jpg', 'Đỗ Nguyễn Bình', '2010-01-22 16:39:00', '', '0000-00-00 00:00:00', 2),
(11, 'Sản phẩm 11', 0, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 4500000, 20, '11.jpg', 'hover11.jpg', 'Đỗ Nguyễn Bình', '2011-01-22 16:39:00', '', '0000-00-00 00:00:00', 2),
(12, 'Sản phẩm 12', 1, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 1000000, 30, '12.jpg', 'hover12.jpg', 'Đỗ Nguyễn Bình', '2012-01-22 16:39:00', '', '0000-00-00 00:00:00', 2),
(13, 'Sản phẩm 13', 1, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 1000000, 20, '13.jpg', 'hover13.jpg', 'Đỗ Nguyễn Bình', '2013-01-22 16:39:00', '', '0000-00-00 00:00:00', 3),
(14, 'Sản phẩm 14', 1, 'Da Thuộc Sài Gòn địa chi mua da bò thuộc chất lượng\nSản phẩm: Da bò Flexible\nXuất xứ:  Da Thuộc Sài Gòn\nMàu sắc có thể đậm hoặc nhạt hơn so với thực tế, tùy theo độ phân giải của màn hình\nĐộ dày: 1.6  – 1.8 mm\nkích thước: 01 pia = 900cm2 (Là đơn vị tính diện tích, không phải tấm da hình vuông)\nTính chất: Đanh dẻo\nNhận sản xuất da theo mẫu khách hàng', 2500000, 10, '14.jpg', 'hover14.jpg', 'Đỗ Nguyễn Bình', '2014-01-22 16:39:00', '', '0000-00-00 00:00:00', 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `san_pham_thanh_toan`
--

CREATE TABLE `san_pham_thanh_toan` (
  `id` int(11) NOT NULL,
  `trang_thai` tinyint(3) NOT NULL DEFAULT 1,
  `id_san_pham` int(11) NOT NULL,
  `so_luong` int(11) NOT NULL,
  `gia_tien` int(11) NOT NULL,
  `id_hoa_don` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `san_pham_thanh_toan`
--

INSERT INTO `san_pham_thanh_toan` (`id`, `trang_thai`, `id_san_pham`, `so_luong`, `gia_tien`, `id_hoa_don`) VALUES
(45, 1, 1, 1, 1000000, 46),
(46, 1, 4, 2, 7000000, 46);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `danh_muc_san_pham`
--
ALTER TABLE `danh_muc_san_pham`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `khach_hang`
--
ALTER TABLE `khach_hang`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `ma_khuyen_mai`
--
ALTER TABLE `ma_khuyen_mai`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `nguoi_quan_tri`
--
ALTER TABLE `nguoi_quan_tri`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `quyen`
--
ALTER TABLE `quyen`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `san_pham`
--
ALTER TABLE `san_pham`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `san_pham_thanh_toan`
--
ALTER TABLE `san_pham_thanh_toan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `danh_muc_san_pham`
--
ALTER TABLE `danh_muc_san_pham`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT cho bảng `hoa_don`
--
ALTER TABLE `hoa_don`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT cho bảng `khach_hang`
--
ALTER TABLE `khach_hang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT cho bảng `ma_khuyen_mai`
--
ALTER TABLE `ma_khuyen_mai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `nguoi_quan_tri`
--
ALTER TABLE `nguoi_quan_tri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `quyen`
--
ALTER TABLE `quyen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `san_pham`
--
ALTER TABLE `san_pham`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT cho bảng `san_pham_thanh_toan`
--
ALTER TABLE `san_pham_thanh_toan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
